package com.example.topscorersbackendapi.service;

import com.example.topscorersbackendapi.service.sport.SportApi;
import com.example.topscorersbackendapi.service.sport.SportDataPoint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
public class SportApiTest {

    @Autowired
    private SportApi sportApi;

    @Test
    void calculate() throws Exception{
        List<SportDataPoint> dataPoints = sportApi.queryForSport();
        assertThat(dataPoints).hasSize(3);
    }

}
