package com.example.topscorersbackendapi.service;

import com.example.topscorersbackendapi.dto.ScorersResult;
import com.example.topscorersbackendapi.service.sport.SportApi;
import com.example.topscorersbackendapi.service.sport.SportDataPoint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.NaN;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;


public class SportCalculatorTest {
    @Autowired
    private SportApi sportApi;


    @Test
    void divide() {
        assertEquals(5,  SportCalculator.calculateAvgGoals(15, 3));
    }

    @Test
    void divideZero() {
        assertEquals(0,  SportCalculator.calculateAvgGoals(0, 3));
    }

    @Test
    void divideZeroByZero() {
        assertEquals(NaN,  SportCalculator.calculateAvgGoals(0, 0));
    }

    @Test
    void divisionWithFraction() {
        assertEquals(1.6666666666666667,  SportCalculator.calculateAvgGoals(5, 3));
    }

    @Test
    void checkArray (){
        List<SportDataPoint> response = new ArrayList<>();
        response.add(0,new SportDataPoint(5));
        response.add(1,new SportDataPoint(5));
        response.add(2,new SportDataPoint(5));
        assertEquals(3, response.stream().count());
        assertEquals(15, response.stream().mapToInt(i -> i.getNumberOfGoals()).sum());
    }


}
