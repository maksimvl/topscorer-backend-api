package com.example.topscorersbackendapi.controller;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

public class RestAssuredScorersControllerTests {
    @Test
    public void ResponseJsonEqualChampionsLeague() {
        get("api/scorers/top3").then().statusCode(200).assertThat()
                .body("competition.name", equalTo("UEFA Champions League"));
    }

    @Test
    public void ValidateResponseinTime() {
        when().get("api/scorers/result").then().time(lessThan(3L), TimeUnit.SECONDS);
    }
}

