package com.example.topscorersbackendapi.controller;

import com.example.topscorersbackendapi.dto.ScorersResult;
import com.example.topscorersbackendapi.service.sport.SportApi;
import com.example.topscorersbackendapi.service.sport.SportDataPoint;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;


import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc

public class ScorersControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private SportApi sportApi;

    @Test
    void filterTop3() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/scorers/top3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("count").value(3));
    }

    @Test
    void validApiResponse() throws Exception {
        Mockito.when(sportApi.queryForSport()).thenReturn(
                List.of(new SportDataPoint(5), new SportDataPoint(3)));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/scorers/result"))
                .andExpect(status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        ScorersResult result = objectMapper.readValue(content, new TypeReference<ScorersResult>() {
        });
        assertThat(result.getNumberOfPlayersInTop()).isEqualTo(2);
        assertThat(result.getTotalGoals()).isEqualTo(8);
        assertThat(result.getAvgGoal()).isEqualTo(4);
    }
}

