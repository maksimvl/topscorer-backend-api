package com.example.topscorersbackendapi.dto;

public class ScorersResult {
    private int numberOfPlayersInTop;
    private int totalGoals;
    private double avgGoal;

    public int getNumberOfPlayersInTop() {
        return numberOfPlayersInTop;
    }

    public void setNumberOfPlayersInTop(int numberOfPlayersInTop) {
        this.numberOfPlayersInTop = numberOfPlayersInTop;
    }

    public int getTotalGoals() {
        return totalGoals;
    }

    public void setTotalGoals(int totalGoals) {
        this.totalGoals = totalGoals;
    }

    public double getAvgGoal() {
        return avgGoal;
    }

    public void setAvgGoal(double avgGoal) {
        this.avgGoal = avgGoal;
    }
}
