package com.example.topscorersbackendapi.service.sport;

public class SportDataPoint {

    private int numberOfGoals;

    public SportDataPoint(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }
}
