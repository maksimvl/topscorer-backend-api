package com.example.topscorersbackendapi.service.sport;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class SportApi {

    @Autowired
    @Qualifier("rapidApiRestTemplate")
    private RestTemplate restTemplate;

    public List<SportDataPoint> queryForSport() {
        ResponseEntity<String> entity = restTemplate.
                getForEntity("https://api.football-data.org/v2/competitions/CL/scorers?limit=3", String.class);
        JSONObject jsonObject = new JSONObject(entity.getBody());
        JSONArray scorers = jsonObject.getJSONArray("scorers");
        List<SportDataPoint> dataPointList = new ArrayList<>();
        for (int i = 0; i < scorers.length(); i++) {
            JSONObject dataPoint = scorers.getJSONObject(i);
            dataPointList.add(new SportDataPoint(dataPoint.getInt("numberOfGoals")));
        }

        return dataPointList;
    }
}
