package com.example.topscorersbackendapi.service;

import com.example.topscorersbackendapi.dto.ScorersResult;
import com.example.topscorersbackendapi.service.sport.SportDataPoint;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SportCalculator {

    public static ScorersResult calculate(List<SportDataPoint> dailyDataPoints) {
        int totalGoals = dailyDataPoints.stream().mapToInt(i -> i.getNumberOfGoals()).sum();
        int totalPlayers = (int) dailyDataPoints.stream().count();

        ScorersResult scorersResult = new ScorersResult();
        scorersResult.setTotalGoals(totalGoals);
        scorersResult.setAvgGoal(calculateAvgGoals(totalGoals, totalPlayers));
        scorersResult.setNumberOfPlayersInTop(totalPlayers);

        return scorersResult;

    }

    static double calculateAvgGoals(double totalGoals, double totalPlayers) {
        return totalGoals / totalPlayers;
    }
}
