package com.example.topscorersbackendapi.service;

import com.example.topscorersbackendapi.dto.ScorersResult;
import com.example.topscorersbackendapi.service.sport.SportApi;
import com.example.topscorersbackendapi.service.sport.SportDataPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SportService {

    @Autowired
    private SportCalculator sportCalculator;

    @Autowired
    private SportApi sportApi;

    public ScorersResult result() {
        List<SportDataPoint> response = sportApi.queryForSport();
        return sportCalculator.calculate(response);
    }
}
