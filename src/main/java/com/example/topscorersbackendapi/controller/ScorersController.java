package com.example.topscorersbackendapi.controller;

import com.example.topscorersbackendapi.dto.ScorersResult;
import com.example.topscorersbackendapi.service.SportService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/api/scorers")
public class ScorersController {

    Logger logger = LoggerFactory.getLogger(ScorersController.class);

    @Autowired
    @Qualifier("rapidApiRestTemplate")
    private RestTemplate restTemplate;

    @Autowired
    private SportService sportService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/top3")
    public ResponseEntity<String> getTop3Scorers() {
        return restTemplate.getForEntity("https://api.football-data.org/v2/competitions/CL/scorers?limit=3",
                String.class);
    }

    @GetMapping("/result")
    public ScorersResult getScorers() throws JsonProcessingException {
        logger.info("Requesting top players");
        ScorersResult result = sportService.result();
        logger.info("Top players result {}", objectMapper.writeValueAsString(result));
        return result;
    }
}
