package com.example.topscorersbackendapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopScorersBackendApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopScorersBackendApiApplication.class, args);
	}

}
